# CSE 535 Information Retrieval Final project #

This project is a [located at](http://ec2-54-213-82-135.us-west-2.compute.amazonaws.com/)
### Features ###

* Search as you type
* Filtering by lang, hashtag, entities(using Solr faceting).
* Content tagging of tweets using Stanford NER
* Graphs

### Frontend ###

* [React](https://facebook.github.io/react/)
* [Chart.js](http://www.chartjs.org/)
* Bootstrap

### Backend ###

* Solr 5.5+
* Spring boot + Jetty
* Gradle